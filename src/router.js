import Vue from 'vue'
import Router from 'vue-router'
import Skills from './components/otherComponent/Skills.vue'
import About from './components/about/about.vue'

Vue.use(Router)

export default new Router({
    
    routes:[
        { path: '/', name: 'skill', component: Skills },
        { path: '/about/', name: 'about',  component: About }  
    ]
    //:name
})